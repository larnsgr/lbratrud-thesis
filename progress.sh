#!/bin/bash
filLines="lines.txt" # Data from pdf2txt stored in here
filPDF="thesis.pdf" # The pdf-file
pdftotext thesis.pdf $filLines # Convert to text file
grep "^[A-Za-z]" $filLines > temp.txt # Extract only lines with actual text in them (no chapter headers etc)
nrLines=`cat temp.txt | wc -l` # Count number of lines
# replace space with newline, remove punctuation and count number of words
nrWords=`cat temp.txt | tr " " "\n" | tr -d '[:punct:]' | wc -w`
# Extract last modification date/time of pdf
modYear=`date -r $filPDF +%Y`
modMonth=`date -r $filPDF +%m`
modDay=`date -r $filPDF +%d`
modHour=`date -r $filPDF +%H`
modMin=`date -r $filPDF +%M`
# create comma-separated entry, check if it exists in the csv-file, and store/plot
# if the entry is not already there.
modLine=`echo "$modDay,$modMonth,$modYear,$modHour,$modMin,$nrLines,$nrWords"`
if grep -q "$modLine" progress.csv; then
	echo "no changes to document"
else
	echo "$modLine" >> progress.csv
	octave -qf progplot.m
	echo "changes updated"
fi
