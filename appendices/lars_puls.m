%% ------------------------------------------------- %%
% Create some pseudo pulses
%% ------------------------------------------------- %%
clear all; close all;
X=1:400; % time
%%%%%%%%%%%%%%%
%% Pulse 1
%%%%%%%%%%%%%%%
Y1=[zeros(1,20) ones(1,185) zeros(1,10)];
% Choose a rise time:
T1=5;
% Implement it:
Y1(21:200)=1-exp(-X(1:180)/T1).*Y1(21:200);
% "fall time":
T2=25;
% Falling
Y1(26:205) =exp(-X(1:180)/T2).*Y1(26:205);
Y1=Y1*300;
%%%%%%%%%%%%%%%
%% Pulse 2
%%%%%%%%%%%%%%%
Y2=[zeros(1,20) ones(1,185) zeros(1,10)];
% Choose a rise time:
T1=10;
% Implement it:
Y2(21:200)=1-exp(-X(1:180)/T1).*Y2(21:200);
% "fall time":
T2=35;
% Falling
Y2(36:205) =exp(-X(1:170)/T2).*Y2(36:205);
Y2=Y2*450;
%%%%%%%%%%%%%%%
%% Pulse 3
%%%%%%%%%%%%%%%
Y3=[zeros(1,120) ones(1,265) zeros(1,95)];
% Choose a rise time:
T1=15;
% Implement it:
Y3(121:300)=1-exp(-X(1:180)/T1).*Y3(121:300);
% "fall time":
T2=60;
% Falling
Y3(140:400) =exp(-X(1:261)/T2).*Y3(140:400);
Y3=Y3*250;
%%%%%%%%%%%%%%%
%% Pulse 4
%%%%%%%%%%%%%%%
Y4=[zeros(1,10) ones(1,150) zeros(1,95)];
% Choose a rise time:
T1=4;
% Implement it:
Y4(11:135)=1-exp(-X(1:125)/T1).*Y4(11:135);
% "fall time":
T2=35;
% Falling
Y4(15:254) =exp(-X(1:240)/T2).*Y4(15:254);
Y4=Y4*500;

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Generate rcu-sh scripts
%%%%%%%%%%%%%%%%%%%%%%%%%%
File = fopen('fakepulse_FEC4_A0_CH2_CH7.script','w');
% Fill Y1 in A0, CH2, CH7, CH1, CH8 in two separate files, I guess
k=1;
% Channel 2
for j=1:4:4*(length(Y1)-1)
	fprintf(File,'w 0x%X 0x28404D\n', j);
	fprintf(File,'w 0x%X 0x%03X # ADDR\n', j+1, k-1);
	fprintf(File,'w 0x%X 0x284047\n', j+2);
	fprintf(File,'w 0x%X 0x%03X # DATA\n', j+3, Y1(k) );
	k=k+1;	
end
k=1;
q=4*(length(Y1)-1);
% Channel 7
for j=1:4:4*(length(Y2)-1)
	fprintf(File,'w 0x%X 0x2840ED\n', j+q);
	fprintf(File,'w 0x%X 0x%03X # ADDR\n', q+j+1, k-1);
	fprintf(File,'w 0x%X 0x2840E7\n', q+j+2);
	fprintf(File,'w 0x%X 0x%03X # DATA\n', q+j+3, Y2(k) );
	k=k+1;	
end
	% Last part
	fprintf(File, 'w 0x%X 0x380000 # End of set of instructions\n',q+j+4);
	fprintf(File, 'w 0x5304 0xF # Execute\n');
	%fprintf(File, 'r 0x2000 1024 -a resmem_pedestal_fill.txt');
fclose(File); % clean up
% New file
File = fopen('fakepulse_FEC4_A0_CH1_CH8.script','w');
k=1;
% Channel 1
for j=1:4:4*(length(Y3)-1)
	fprintf(File,'w 0x%X 0x28402D\n', j);
	fprintf(File,'w 0x%X 0x%03X # ADDR\n', j+1, k-1);
	fprintf(File,'w 0x%X 0x284027\n', j+2);
	fprintf(File,'w 0x%X 0x%03X # DATA\n', j+3, Y3(k) );
	k=k+1;	
end
k=1;
q=4*(length(Y3)-1);
% Channel 8
for j=1:4:4*(length(Y4)-1)
	fprintf(File,'w 0x%X 0x28410D\n', j+q);
	fprintf(File,'w 0x%X 0x%03X # ADDR\n', q+j+1, k-1);
	fprintf(File,'w 0x%X 0x284107\n', q+j+2);
	fprintf(File,'w 0x%X 0x%03X # DATA\n', q+j+3, Y4(k) );
	k=k+1;	
end
	% Last part
	fprintf(File, 'w 0x%X 0x380000 # End of set of instructions\n',q+j+4);
	fprintf(File, 'w 0x5304 0xF # Execute\n');
fclose(File); % clean up

% Fill with zeroes
File = fopen('all_channels_zero.script','w');
i=1;
for j=1:4:4*(1023)
	fprintf(File,'w 0x%X 0x24000D\n', j);
	fprintf(File,'w 0x%X 0x%03X # ADDR\n', j+1, i-1);
	fprintf(File,'w 0x%X 0x240007\n', j+2);
	fprintf(File,'w 0x%X 0x000 # DATA\n', j+3);
	i=i+1;	
end
	fprintf(File, 'w 0x%X 0x380000 # End of set of instructions\n',j+4);
	fprintf(File, 'w 0x5304 0xF # Execute\n');
%	fprintf(File, 'r 0x2000 1024 -a resmem_pedestal_fill.txt');
	fprintf(File, 'w 0x0 0x24000D\n');
	fprintf(File, 'w 0x1 0x3FF\n');
	fprintf(File, 'w 0x2 0x240007\n');
	fprintf(File, 'w 0x3 0x000\n');
	fprintf(File, 'w 0x4 0x380000\n');
	fprintf(File, 'w 0x5304 0xF');

fclose(File);

%% Plot, testing stuff
%{
subplot(4,2,1), plot(Y1);
subplot(4,2,2), plot(Y2);
subplot(4,2,3), plot(Y3);
subplot(4,2,4), plot(Y4);
%}
