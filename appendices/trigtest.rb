require 'rubygems'
require 'net/ssh'

DAQ_PC     ='drorc'
TRIGGER_PC ='vme1'
RCU_DCS    ='dcs0193'
BUSYBOX_DCS='dcs1226'

RORC_MINOR=2    # DDL link minor number
ENV_PARTITION=1 # partition number to obtain DDL link from environment.

##################
### OPEN LINKS ###
##################
print "Opening links to DAQ..."
daq    =Net::SSH.start(DAQ_PC     ,'date', :password => 'pass')
print " RCU DCS board..."
rcu    =Net::SSH.start(RCU_DCS    ,'root', :password => 'pass')
print " LTU..."
trigger=Net::SSH.start(TRIGGER_PC ,'ltu', :password => 'pass')
puts " done!"

#########################
### CONFIGURE RCU     ###
#########################
print "Configuring RCU... "
#rcu.exec!('/mnt/kjekspc7/lars/configs/ctp_51sam11pre.script')
#rcu.exec!('/usr/local/sbin/rcu-sh fw r')
print "fw reset.. "
rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/alme/reset.script')
rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/lars/sparse/all_channels_zero.script')
print "zero all channels... "
#rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/lars/sparse/fakepulse_FEC4_A0_CH1_CH8.script')
print "FEC4,A0,CH1,CH8... "
#rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/lars/sparse/fakepulse_FEC4_A0_CH2_CH7.script')
print "FEC4,A0,CH2,CH7... "
#rcu.exec!('/bin/sh /mnt/kjekspc7/lars/sparse/tja.sh')
rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/alme/reset.script')
#rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/lars/sparse/softsparse_51sam11pre.script')

#rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/lars/configs/ctp_51sam11pre.script')
lars=rcu.exec!('/bin/sh /mnt/kjekspc7/lars/configs/nei.sh')
#print "#{lars}"
rcu.exec!('/usr/local/sbin/rcu-sh w 0x5103 0x4') # sparse
rcu.exec!('/usr/local/sbin/rcu-sh 0x510A 0x3E8') # SCEVL exec time
rcu.exec!('/usr/local/sbin/rcu-sh 0x800C 0x1')# Slow control clock
#rcu.exec!('/usr/local/sbin/rcu-sh w 0x5000 128 0xffffffff') # reset HTLM
#rcu.exec!('/usr/local/sbin/rcu-sh w 0x2000 8 0x0')
#rcu.exec!('/usr/local/sbin/rcu-sh w 0x0 0x004051')
#rcu.exec!('/usr/local/sbin/rcu-sh w 0x1 0x380000')
#print "#{lars}"
rcu.exec!('/usr/local/sbin/rcu-sh b /mnt/kjekspc7/lars/sparse/lass.scr')

print "readout config... "
puts "done!"
#########################
### CONFIGURE TRIGGER ###
#########################/usr/local/sbin/rcu-sh
# rcu.exec!('/usr/local/sbin/rcu-sh c 0x5306')
#rcu.exec!('/usr/local/sbin/rcu-sh 0x5304 0xF')
#lrs=rcu.exec!('/usr/local/sbin/rcu-sh r 0x2000 4')
#puts "lrs: #{lrs}"
#   open the read-out link
  print " open DDL..."
  bytes_received=-1
  rorc_receive=daq.exec!("/date/rorc/Linux/rorc_receive -P1 -m#{RORC_MINOR} -e1 -X0 -F1 -K/heim/date/21may/sparsetest.dat") do |ch,stream,data|
    if stream == :stderr
      puts "warning: error in rorc_receive: #{data}"
    else
      #puts data
      if data =~ /RDYDX/
        print " trigger!..."
#        trigger_channel.send_data "SLMswstart(1,0)\n"


	rcu.exec!("/usr/local/sbin/rcu-sh c 0x5306")

#rcu.exec!('/usr/local/sbin/rcu-sh w 0x2000 8 0x0')
#rcu.exec!('/usr/local/sbin/rcu-sh w 0x0 0x004051')
#rcu.exec!('/usr/local/sbin/rcu-sh w 0x1 0x380000')

        rcu.exec!("/usr/local/sbin/rcu-sh w 0x5304 0xF")
        scevl=rcu.exec!('/usr/local/sbin/rcu-sh r 0x2000 32 -f "0x%06x"')
	puts "sent..."
        puts "0x11: #{scevl}"
      end
      if data =~ /moved \d* byte/
        bytes_received=data.scan(/moved \d* byte/).first.split[1].to_i
      end
    end
    end
# Check events

 #       rcu.exec!('/usr/local/sbin/rcu-sh 0x5304 0xF')
#        scevl=rcu.exec!('/usr/local/sbin/rcu-sh r 0x2000 4')
#puts "0x11: #{scevl}"

print "check event data... "
mismatch=daq.exec!("ruby -I/heim/date/work/ruby /heim/date/work/ruby/difftest.rb /heim/date/21may/fulltest.dat /heim/date/21may/sparsetest.dat")
puts mismatch
if mismatch =~ /missmatch/
   puts "written data file: sparsetest.dat"
   puts "read data file: fulltest.dat"
end
print " read RCU RDO_TIME... "
rdo_time=Integer(rcu.exec!('/usr/local/sbin/rcu-sh r 0x511C -f "0x%08x"'))
print "#{rdo_time}"
hitm4=rcu.exec!('/usr/local/sbin/rcu-sh r 0x5010 4 -f "0x%08x"')
hitm14=rcu.exec!('/usr/local/sbin/rcu-sh r 0x5038 4 -f "0x%08x"')
puts "Hit list memory... FEC4: "
puts " #{hitm4}"
puts "Hit list memory... FEC14: "
puts " #{hitm14}"
 
# Read readout time from RCU


#print " read RCU..."
#  rdo_time=Integer(rcu.exec!('/usr/local/sbin/rcu-sh r 0x511C -f "0x%x"'));
#puts "Readout time: #{rdo_time}";
