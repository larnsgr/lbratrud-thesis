%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % This script is for plotting thesis progress
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; close all;
%% Read the csv and create vectors
a=csvread('progress.csv');
aDay=a(:,1);
aMonth=a(:,2);
aYear=a(:,3);
aHour=a(:,4);
aMin=a(:,5);
aDate=datenum(aYear(:),aMonth(:),aDay(:),aHour(:),aMin(:)); 
aLines=a(:,6);
aWords=a(:,7);

%% General stuff
%     mm dd
xx = [01 01;
      01 15;
      01 29;
      02 12;
      02 26;
      03 12;
      03 26;
      04 09;
      04 23;
      05 07;
      05 24;];
xxD = datenum(2013,xx(:,1),xx(:,2));

%% Plot
figure
%plot(aDate,aLines);
[AX,hline1,hline2] = plotyy(aDate,aLines,aDate,aWords,'plot','plot');
% make nice axes

set(AX(1),'XLim',[datenum(2013,01,01) datenum(2013,05,24)]);
set(AX(1),'Xtick',xxD);
set(AX(1),'XTickLabel',datestr(xxD,'dd-mmm'));
%get(AX(2));
set(AX(2),'XLim',[datenum(2013,01,01) datenum(2013,05,24)]);
set(AX(2),'Xtick',xxD);
set(AX(2),'XTickLabel',datestr(xxD,'dd-mmm'));
axis square; % fix for octave, uncomment is used with matlab
title('Thesis -- Writing Progress');
xlabel('Date');
set(get(AX(1),'Ylabel'),'String','Approximitely Number of Lines');
set(get(AX(2),'Ylabel'),'String','Approximitely Number of Words');
%axes(AX(1)); ylabel("Approximitely Number of Lines in Document");
%axes(AX(2)); ylabel("Approximitely Number of Words in Document");

% Save in different formats
print -deps progress.eps
print -dpdf progress.pdf
print -dpng progress.png
